let g:terraform_fmt_on_save = 1

let g:deoplete#enable_at_startup = 1
" call deoplete#custom#option({
" \ 'auto_complete': v:false,
" \ 'smart_case': v:true,
" \ 'max_list': 20,
" \ })
" " run deoplete completion on C-n
" inoremap <expr> <C-n> pumvisible() ? "\<C-n>" : deoplete#manual_complete()

let g:deoplete#sources#rust#rust_source_path = "/home/eon/vcs/rust/src"

let g:echodoc#enable_at_startup = 1
let g:echodoc#type = "floating"
highlight link EchoDocFloat Pmenu

let g:jedi#show_call_signatures = "2"
let g:jedi#popup_on_dot = 0
let g:jedi#popup_select_first = 0
" handled by deoplete
let g:jedi#completions_enabled = 0

let g:go_highlight_functions = 1
let g:go_highlight_types = 1
let g:go_fmt_command = "gofmt"

let g:toggle_list_no_mappings = 1

let g:fzf_preview_window = ''

let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Label'],
  \ 'fg+':     ['fg', 'Label'],
  \ 'bg+':     ['bg', 'Normal'],
  \ 'hl+':     ['fg', 'Number'],
  \ 'info':    ['fg', 'Comment'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Function'],
  \ 'pointer': ['fg', 'Label'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" An action can be a reference to a function that processes selected lines
function! s:build_quickfix_list(lines)
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction

let g:fzf_action = {
  \ 'ctrl-q': function('s:build_quickfix_list'),
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit' }

let g:fzf_layout = { 'window': 'call CenteredFloatingWindow()' }
" floating fzf window with borders
function! CenteredFloatingWindow()
    let width = min([&columns - 4, max([80, &columns - 20])])
    let height = min([&lines - 4, max([20, &lines - 10])])
    let top = ((&lines - height) / 2) - 1
    let left = (&columns - width) / 2
    let opts = {'relative': 'editor', 'row': top, 'col': left, 'width': width, 'height': height, 'style': 'minimal'}
    let top = "╭" . repeat("─", width - 2) . "╮"
    let mid = "│" . repeat(" ", width - 2) . "│"
    let bot = "╰" . repeat("─", width - 2) . "╯"
    let lines = [top] + repeat([mid], height - 2) + [bot]
    let s:buf = nvim_create_buf(v:false, v:true)
    call nvim_buf_set_lines(s:buf, 0, -1, v:true, lines)
    call nvim_open_win(s:buf, v:true, opts)
    set winhl=Normal:Floating
    let opts.row += 1
    let opts.height -= 2
    let opts.col += 2
    let opts.width -= 4
    call nvim_open_win(nvim_create_buf(v:false, v:true), v:true, opts)
    au BufWipeout <buffer> exe 'bw '.s:buf
endfunction

if (has("termguicolors"))
  set termguicolors
endif
colorscheme base16-oceanicnext

let mapleader = ","

" grep
if executable('ag')
  set grepprg=ag\ --vimgrep
endif
nnoremap <leader>g :Grep<Space>
nnoremap <silent> <leader>gr :Grep <cword><CR>

" find files
nnoremap <leader>f :Find<Space>

" clear search
nnoremap <leader>c :noh<CR>

" remove trailing spaces
nnoremap <leader>w :%s/\s\+$//e<CR>

" tagbar
nnoremap <leader>l :TagbarToggle<CR>

" lists
nnoremap <leader>a :call ToggleLocationList()<CR>
nnoremap <leader>q :call ToggleQuickfixList()<CR>

" fuzzy search
nmap <leader>o :GFiles<CR>
nmap <leader>p :Files<CR>
nmap <leader><space> :Buffers<CR>

" search in nixpkgs
nnoremap <leader>nix :call fzf#vim#files('~/vcs/nixpkgs', {}, 0)<CR>

" edit neovim configuration
nnoremap <leader>vim :split ~/.config/nvim/init.vim<CR>

" indent motion
map [[ <Plug>(IndentWiseBlockScopeBoundaryBegin)
map ]] <Plug>(IndentWiseBlockScopeBoundaryEnd)

" move between windows
nnoremap <C-Left> <C-w>h
nnoremap <C-Down> <C-w>j
nnoremap <C-Up> <C-w>k
nnoremap <C-Right> <C-w>l

" disable hex mode
map Q <Nop>
" disable help
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>

" vim-sandwich
nmap s <Nop>
xmap s <Nop>

" buffers
" switch like tabs in gtk apps/firefox
nmap <C-PageUp> :bprevious<cr>
nmap <C-PageDown> :bnext<cr>
imap <C-PageUp> <ESC>:bprevious<cr>
imap <C-PageDown> <ESC>:bnext<cr>
nmap <C-c> :bdelete<CR>
nmap <C-Esc> :bdelete!<CR>

" esc to go to normal mode in terminal
tnoremap <Esc> <C-\><C-n>
" escape from terminal easily
tnoremap <C-Left> <C-\><C-n><C-w>h
tnoremap <C-Down> <C-\><C-n><C-w>j
tnoremap <C-Up> <C-\><C-n><C-w>k
tnoremap <C-Right> <C-\><C-n><C-w>l

function! GitBranch()
    let l:branch = fugitive#head()
    return strlen(l:branch) > 0 ? "\ \ " . l:branch . "\ " : ""
endfunction

function! ActiveStatus()
    let statusline=""
    let statusline.="%<"
    let statusline.="%#CursorLineNr#"
    let statusline.="%f"
    let statusline.="%*"
    let statusline.="%#DiffText#"
    let statusline.="%{GitBranch()}"
    let statusline.="%*"
    let statusline.="%h%m%r"
    let statusline.="%="
    let statusline.="\ %-14.(%l/%L,%c%V%)"
    let statusline.="\ %P"
    return statusline
endfunction

function! InactiveStatus()
    let statusline="%<"
    let statusline.="%f\ %h%m%r"
    return statusline
endfunction

set statusline=%!ActiveStatus()

augroup status
  autocmd!
  autocmd WinEnter * setlocal statusline=%!ActiveStatus()
  autocmd WinLeave * setlocal statusline=%!InactiveStatus()
augroup END

set clipboard+=unnamedplus   " shared clipboard
set nobackup                " disable backup files (filename~)
set showmatch               " show matching brackets (),{},[]
set wildmode=list:longest
set wildignore+=*\\.pyc\\*,*\\.mo\\*,*\\Makefile.in\\*
set expandtab
set softtabstop=4 ts=4 sw=4
set backspace=indent,eol,start
set ignorecase              " case-insensitive search
set smartcase               " upper-case sensitive search
set listchars=tab:▸\ ,eol:¬
set mouse=a
set inccommand=split
set splitbelow          " Horizontal split below current.
set splitright          " Vertical split to right of current.
" don't show the docstring on completion
set completeopt-=preview
set updatetime=100
set hidden
set scrolloff=10
set nofoldenable
set winblend=10         " floating win transparancy

hi VertSplit guibg=#343D46 guifg=#343D46
hi StatusLine guibg=#343D46
hi StatusLineNC guifg=#4F5B66
hi Search gui=underline,bold cterm=underline,bold
  \ ctermfg=none ctermbg=none guifg=#e5c07b guibg=none
" hightlight trailing whitespaces
hi ExtraWhitespace ctermbg=8 guibg=#EC5F67

if !exists("autocommands_loaded")
    command -nargs=+ -complete=file -bar Grep silent! grep! <args> | botright cwindow
    command -nargs=1 -bar Find
        \ call setloclist(0, map(systemlist("fd -p -t f <args>"), '{"filename": v:val, "lnum": 1}'), ' ', "Find '<args>'") | botright lwindow
    " match trailing spaces and spaces before a tab
    au BufWinEnter * match ExtraWhitespace /\s\+$\| \+\ze\t/
    au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    au InsertLeave * match ExtraWhitespace /\s\+$\| \+\ze\t/
    au BufWinLeave * call clearmatches()
    " go to def
    au FileType python nnoremap <buffer> <C-]> :call jedi#goto()<CR>
    " set cue ft properly
    au BufWinenter *.cue setlocal ft=cue
    " tabs
    au FileType c,cue,make set noic cin noexpandtab
    au FileType nix,json,yaml,javascript,xml,html,ruby,tf set ts=2 sw=2 softtabstop=2
    au FileType nix let &l:formatprg="nixpkgs-fmt"
    au FileType html let &l:formatprg="pandoc --from=html --to=markdown | pandoc --from=markdown --to=html"
    au FileType json let &l:formatprg="jq ."
    au FileType gitcommit,mail,markdown setlocal spell
    " put help in vertical mode
    " au FileType help wincmd L
    au FileType mail colorscheme base16-unikitty-light
      \| setl tw=0 linebreak wrap laststatus=0
    " automatically be in insert mode when entering terminal
    au BufEnter,TermOpen * if &buftype == 'terminal' | :startinsert | endif
    " mode for mails
    au BufRead,BufNewFile .followup,.article,.letter,/tmp/mutt*,*astroid@* set ft=mail
    " clean fugitive buffers automatically
    " au BufReadPost fugitive://* set bufhidden=delete
    " run checkers
    au! BufWritePost * Neomake
    " compile gschema on save
    au BufWritePost *.gschema.xml !glib-compile-schemas %:p:h
    let autocommands_loaded = 1
endif
