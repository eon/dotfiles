{ pkgs
, stdenv
, fetchFromGitHub
, pkgconfig
, cmake
, meson
, ninja
, wayland
, wayland-protocols
, systemd
, pipewire
, libdrm
}:

stdenv.mkDerivation rec {
  name = "xdg-desktop-portal-wlr";

  src = fetchFromGitHub {
    owner = "emersion";
    repo = name;
    rev = "dfa0ac704064304824b6d4fea7870d33359dcd15";
    sha256 = "0k73nyd9z25ph4pc4vpa3xsd49b783qfk1dxqk20bgyg1ln54b81";
  };

  nativeBuildInputs = [ meson ninja pkgconfig wayland-protocols ];

  buildInputs = [ systemd wayland pipewire libdrm ];

  meta = with stdenv.lib; {
    description = "Desktop integration portals for sandboxed apps";
    maintainers = with maintainers; [ eonpatapon ];
    platforms = platforms.linux;
    license = licenses.lgpl21;
  };
}
