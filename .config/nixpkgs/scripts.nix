{ pkgs, config, ... }:

{

  home.packages = [

    (pkgs.writeShellScriptBin "IRC" ''
      ${pkgs.urxvt}/bin/urxvt -e ${pkgs.mosh}/bin/mosh fion.patapon.info tmux a
    '')

    (pkgs.writeShellScriptBin "fzy-pass" ''
      gopass=${pkgs.gopass}/bin/gopass

      function _find() {
           $gopass ls -f | ${pkgs.fzy}/bin/fzy -l 100
      }

      function _main() {
          clear
          pass_name=$(_find)
          [ ! $? -eq 0 ] && exit 0
          $gopass show -C "$pass_name" 2>/dev/null | tail -n+3
          read -n 1 -r
          _main
      }

      _main
    '')

  ];

}
