{ config, pkgs, ... }:

{

  programs.astroid = {
    enable = true;
    externalEditor = "${pkgs.termite}/bin/termite -c ${./conf/termite-white} -e '${pkgs.neovim}/bin/vim %1'";
    extraConfig = {
      startup.queries = {
        inbox = "tag:inbox and NOT tag:monitoring";
        lists = "tag:list and NOT tag:muted";
        github = "tag:github and tag:unread and not tag:skydive";
        monitoring = "tag:monitoring and NOT tag:archive";
        todo = "tag:todo and NOT tag:archive";
        impots = "tag:impots and tag:unread and NOT tag:archive";
      };
      thread_index = {
        page_jump_rows = "6";
        sort_order = "newest";
        cell = {
          font_description = "Hack Nerd Font Mono 10.5";
          authors_length = "70";
          authors_color_selected = "#fefefe";
          subject_color = "#272822";
          subject_color_selected = "#fefefe";
          background_color_selected = "";
          background_color_marked = "#fff584";
          background_color_marked_selected = "#bcb559";
          tags_length = "200";
          tags_upper_color = "#e5e5e5";
          tags_lower_color = "#333333";
          tags_alpha = "1";
          hidden_tags = "attachment,flagged,signed,unread,list,me";
        };
      };
      mail.reply.quote_processor = "${pkgs.w3m}/bin/w3m -dump -T text/html";
      mail.close_on_success = "true";
      thread_view.preferred_html_only = "true";
      poll.interval = 60;
      accounts.patapon.save_sent_to = "/home/eon/Mail/eon@patapon.info/Sent/cur";
    };
    pollScript = ''
      mbsync -a
      notmuch new
    '';
  };
  programs.msmtp.enable = true;
  programs.mbsync.enable = true;
  programs.notmuch = {
    enable = true;
    new.tags = [ "new" ];
    hooks.postNew = builtins.readFile ./conf/notmuch-tag;
  };

  accounts.email = {
    maildirBasePath = "${config.home.homeDirectory}/Mail";
    accounts = {
      patapon = {
        primary = true;
        address = "eon@patapon.info";
        passwordCommand = "${pkgs.pass}/bin/pass accounts/patapon.info";
        realName = "Jean-Philippe Braun";
        userName = "eon@patapon.info";
        imap = {
          host = "mail.patapon.info";
          port = 143;
          tls.useStartTls = true;
        };
        smtp = {
          host = "mail.patapon.info";
          port = 587;
          tls.useStartTls = true;
        };
        folders.inbox = "INBOX";
        maildir.path = "eon@patapon.info";
        astroid.enable = true;
        msmtp.enable = true;
        notmuch.enable = true;
        mbsync = {
          enable = true;
          create = "both";
          expunge = "both";
          remove = "both";
          patterns = [ "INBOX" "Sent" "Junk" "lists" "monitoring" ];
        };
      };
      gmail = {
        address = "braun.jp@gmail.com";
        passwordCommand = "${pkgs.pass}/bin/pass accounts/gmail.com";
        realName = "Jean-Philippe Braun";
        userName = "braun.jp@gmail.com";
        flavor = "gmail.com";
        maildir.path = "braun.jp@gmail.com";
        astroid.enable = true;
        msmtp.enable = true;
        notmuch.enable = true;
        mbsync = {
          enable = true;
          create = "both";
          expunge = "both";
          remove = "both";
          patterns = [ "INBOX" "[Gmail]/Messages envoy&AOk-s" ];
        };
      };
    };
  };


}
