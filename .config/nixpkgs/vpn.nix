{ config, pkgs, ... }:

/*
  10.114.49.66    exchange-eme3.itn.ftgroup exchange-eme2.itn.ftgroup exchange-eme6.itn.ftgroup
  10.242.80.230   agresso-web.grenoble.francetelecom.fr
  10.242.190.97   agresso-web.si.fr.intraorange
  10.242.191.5    chronos.si.fr.intraorange
  10.242.80.240   password.grenoble.francetelecom.fr

  https://mpa.itn.ftgroup/QPM/User/Identification/
  https://motdepassesubs.si.fr.intraorange/private/Login
*/

let

  vpn-config = pkgs.writeTextFile {
    name = "config";
    text = ''
      #no-cert-check
      reconnect-timeout 60
      user jean-philippe.braun
      authgroup CLOUDWATT
    '';
  };

  vpn-cloudwatt = pkgs.writeShellScriptBin "vpn-cloudwatt" ''
    set -e
    export password="$(pass accounts/cloudwatt.com | head -n1)$(pass otp otp/cloudwatt/TOTP0082804E)"
    sudo sh -E -c "echo $password | ${pkgs.openconnect}/bin/openconnect --passwd-on-stdin --config ${vpn-config} vpn.corp.cloudwatt.com"
  '';

  vpn-orange = pkgs.writeShellScriptBin "vpn-orange" ''
    set -e
    profile=''${1:-externe}
    login=$(pass accounts/orange.com/ad-subs | grep login | sed 's/login: \(.*\)/\1/')
    pass=$(pass accounts/orange.com/ad-subs | head -n1)
    ${pkgs.ike}/bin/ikec -r $profile -u $login -p $pass -a
  '';

in {

  home.packages = [
    vpn-cloudwatt vpn-orange
  ];

}
