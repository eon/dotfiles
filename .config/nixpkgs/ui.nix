{ config, pkgs, ... }: {

  home.packages = with pkgs; [
    hicolor-icon-theme
    hack-font hack-nerd-font cantarell-fonts noto-fonts noto-fonts-emoji
  ];

  fonts.fontconfig.enable = true;

  # Fix scaling issues with firefox, qt apps
  programs.bash.sessionVariables = {
    GDK_DPI_SCALE = 1;
    GDK_SCALE = 1;
  };

  gtk = {
    enable = true;
    font.name = "Cantarell Regular 11";
    gtk3.extraConfig = {
      gtk-xft-antialias = 1;
      gtk-xft-hinting = 1;
      gtk-xft-hintstyle = "hintslight";
      gtk-xft-rgba = "rgb";
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
  };

}
