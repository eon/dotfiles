{ config, pkgs, ... }: {

  home.packages = with pkgs; [
    mkvtoolnix
    audacity
    # pitivi
  ];

  programs.obs-studio = {
    enable = true;
    plugins = [ pkgs.obs-wlrobs ];
  };

}
