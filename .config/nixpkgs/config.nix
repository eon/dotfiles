{

  allowUnfree = true;

  pulseaudio = true;

  chromium = {
    commandLineArgs = "--force-device-scale-factor=1";
  };

  oraclejdk.accept_license = true;

}
