{ config, pkgs, ... }:

with pkgs.lib;

{

  imports = [
    ./vpn.nix
    ./mail.nix
    ./web.nix
    ./shell.nix
    ./sway.nix
    ./ui.nix
    ./sound.nix
    ./video.nix
    ./scripts.nix
  ];

  programs.home-manager.enable = true;

  home.packages = with pkgs; [
    unzip htop ag fd direnv file ack fzy
    xterm termite urxvt alacritty
    dnsutils wireshark tcpdump
    mosh
    mumble
    shellcheck
    gnumake
    neovim
    font-manager
    pavucontrol ncmpcpp playerctl mpc_cli
    jq yq
    nixpkgs-fmt
    tmate tmux
    vifm
    git tig gitAndTools.hub gist
    asciinema
    # pijul
    zathura
    dfeet
    cachix
    python38Packages.grip # md preview
    niv
    wget
    cntr
    libreoffice
    xdg_utils
    gcolor3
    celluloid
    mpv
    vlc
    minikube docker-machine-kvm2
    qutebrowser
    go
    anydesk
    gimp
    # (texlive.combine {
    #   inherit (texlive) scheme-small moderncv fontawesome footmisc;
    # })
    nextcloud-client
    pwgen
    darktable
  ] ++ (with pkgs.gnome3; [
    file-roller eog evince gnome-calculator nautilus
    adwaita-icon-theme gedit
  ]);

}
