self: super:

with super.lib;

{

  nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
    pkgs = self;
  };

  waybar = super.waybar.overrideAttrs(old: {
    src = super.fetchFromGitHub {
      owner = "Alexays";
      repo = "Waybar";
      rev = "dc625490f8b7ea5d08d74468e3733ffce66cc3c1";
      sha256 = "0yh2ajpbb6rhp48dninmaw3nflj6h9pdychr8svshgklg2b9dmcm";
    };
    patches = [ ../packages/waybar.patch ];
  });

  cue_0_3_0 = super.buildGoModule rec {
    pname = "cue";
    version = "0.3.0";
    src = super.fetchFromGitHub {
      owner = "cuelang";
      repo = "cue";
      rev = "e77ccb1c2e960603a4cdd3c127767002af6a167b";
      sha256 = "1d839wylk478cn113fd0k0ihlsszmzbq7raiam8gaizfpjjwzld7";
    };
    vendorSha256 = "0xdrmwdqpdz7l364ll6kk8kbgsc6hwkjakbz2yrbphm1l7yc8kdp";
    subPackages = [ "cmd/cue" ];
    buildFlagsArray = [
      "-ldflags=-X cuelang.org/go/cmd/cue/cmd.version=${version}"
    ];
  };

  astroid = super.astroid.overrideAttrs (old: {
    src = super.fetchFromGitHub {
      owner = "astroidmail";
      repo = "astroid";
      rev = "0723bf78587eb9c36fa304e33e53b0dfae112589";
      sha256 = "0b3h1skqv9q2xm1k9hwzhpjira6yb2h02j07d1fi7hcli6923b8p";
    };
  });

  swayctl = import (builtins.fetchGit {
    url = "http://github.com/nlewo/swayctl.git";
    ref = "master";
  }) {};

  vimPlugins = super.vimPlugins // {

    # vim-cue = super.vimUtils.buildVimPluginFrom2Nix {
    #   pname = "vim-cue";
    #   version = "2020-02-14";
    #   src = super.fetchFromGitHub {
    #     owner = "jjo";
    #     repo = "vim-cue";
    #     rev = "482f0b1df03f59bbd1bf4bca99314770289a7f35";
    #     sha256 = "05c9amxpn4zyb8hvcnzrzq43r70hzs32rhn5rjaf5jflgmwaph0q";
    #   };
    # };

    # deoplete-jedi = super.vimUtils.buildVimPluginFrom2Nix {
    #   pname = "deoplete-jedi";
    #   version = "2020-08-06";
    #   src = super.fetchFromGitHub {
    #     owner = "deoplete-plugins";
    #     repo = "deoplete-jedi";
    #     rev = "2786058b9022ce6e50db7f75088e69e07185e52c";
    #     sha256 = "0myn6rgwq7yd3hpxdxa7kj3dlk2x9ljqlznqg95qcm8i0w53z1wg";
    #     fetchSubmodules = true;
    #   };
    #   meta.homepage = "https://github.com/deoplete-plugins/deoplete-jedi/";
    # };

  };

  rxvt_unicode = super.rxvt_unicode.overrideAttrs (old: {
    patches = [
      ../packages/rxvt-unicode/font-width-fix.patch
      ../packages/rxvt-unicode/line-spacing-fix.patch
      ../packages/rxvt-unicode/sgr-mouse-mode.patch
      ../packages/rxvt-unicode/24-bit-color.patch
    ];
    preConfigure = ''
      mkdir -p $terminfo/share/terminfo
      configureFlags="--with-terminfo=$terminfo/share/terminfo --enable-24-bit-color --enable-combining --enable-font-styles --enable-iso14755 --enable-keepscrolling --enable-lastlog --enable-mousewheel --enable-pointer-blank --enable-text-blink --enable-selectionscrolling --enable-slipwheeling --enable-smart-resize --enable-utmp --enable-wtmp --enable-xim --enable-xft --enable-perl --enable-frills --enable-unicode3 --enable-vanilla --disable-fading --disable-rxvt-scroll --disable-xterm-scroll --disable-next-scroll --disable-startup-notification --disable-transparency"
      export TERMINFO=$terminfo/share/terminfo # without this the terminfo won't be compiled by tic, see man tic
      NIX_CFLAGS_COMPILE="$NIX_CFLAGS_COMPILE -I${self.freetype.dev}/include/freetype2"
      NIX_LDFLAGS="$NIX_LDFLAGS -lfontconfig -lXrender -lpthread "
      mkdir -p $out/$(dirname ${self.perl.libPrefix})
      ln -s $out/lib/urxvt $out/${self.perl.libPrefix}
    '';
  });

  urxvt = super.rxvt_unicode-with-plugins.override {
    configure = { ... }: {
      plugins = [ self.urxvt_perl self.urxvt_perls self.urxvt_font_size ];
    };
  };

  neovim =
    let
      # python-vim-shell = super.writeShellScriptBin "python-vim-shell" ''
      #   PYTHONPATH="${self.python3.withPackages (p: [ p.pynvim ])}/${self.python3.sitePackages}:$PYTHONPATH" $(which python) "$@"
      # '';
      nvim = super.neovim.override {
        configure = {
            # if exists("$IN_NIX_SHELL")
            #   let python_path = system("which python")[:-2]
            #   if ! v:shell_error
            #     let g:python3_host_prog = "${python-vim-shell}/bin/python-vim-shell"
            #   endif
            # endif
            # let g:deoplete#sources#rust#racer_binary='${self.rustracer}/bin/racer'
          customRC = ''
            source /home/eon/.config/nvim/init.vim
          '';
          packages.a = with self.vimPlugins; {
            start = [
              base16-vim
              fzf-vim
              fugitive
              vim-gitgutter
              readline-vim
              deoplete-nvim
              echodoc-vim
              vim-commentary
              vim-unimpaired
              vim-sandwich
              vim-vinegar
              vim-togglelist
              tagbar
              neomake
              deoplete-notmuch
              vim-cue
              vim-go
              deoplete-go
              jedi-vim
              deoplete-jedi
              vim-json
              vim-nix
              vim-markdown
              vim-terraform
              # rust-vim
              # deoplete-rust
            ];
          };
        };
      };
      # Binaries to inject in nvim PATH
      binPath = super.stdenv.lib.makeBinPath (with self; [
        fd fzy
        go gotools golangci-lint gocode goimports go-motion gotags gogetdoc gopls
        gcc universal-ctags
        nixfmt
      ]);
    in super.stdenv.mkDerivation {
      name = "neovim";
      buildInputs = [ self.makeWrapper ];
      phases = [ "installPhase" ];
      installPhase = ''
        mkdir -p $out/bin
        ln -s ${nvim}/bin/nvim $out/bin/nvim
        ln -s $out/bin/nvim $out/bin/vim
        wrapProgram $out/bin/nvim --prefix PATH ":" ${binPath}
      '';
    };

  hack-nerd-font = super.fetchzip {
    name = "hack-nerd-font";
    url = https://github.com/ryanoasis/nerd-fonts/releases/download/v2.0.0/Hack.zip;
    sha256 = "14sjpybl4cvq205s36c3988d2x6k6n0zv6xndszd8b7d8cr60kym";
    postFetch = ''
      mkdir -p $out/share/fonts
      unzip -j $downloadedFile \*.ttf -d $out/share/fonts/hack
    '';
  };

}
