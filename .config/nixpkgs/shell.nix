{ config, pkgs, ... }:

let

  initExtraCommon = ''
    function set_win_title(){
      echo -ne "\033]0;$USER@$HOSTNAME: $PWD\007"
    }
  '';

  sessionVariables = {
    FZF_DEFAULT_COMMAND = "${pkgs.fd}/bin/fd --hidden --exclude '.git' .";
    EDITOR = "${pkgs.neovim}/bin/nvim";
    GOPATH = "$HOME/go";
    KSWITCH_TRACE = 1;
  };

  shellAliases = {
    j = "z";
    ls = "${pkgs.exa}/bin/exa";
    ll = "ls -l";
    tree = "ls --tree";
    nssh = "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null";
    config = "${pkgs.git}/bin/git --git-dir=$HOME/vcs/dotfiles --work-tree=$HOME";
    k = "kubectl";
  };

  mkenv = pkgs.writeShellScriptBin "mkenv" ''
    set -ue
    set -o pipefail
    pkgs=(''${@})
    for i in "''${!pkgs[@]}"; do
      pkgs[i]="nixpkgs.''${pkgs[i]}"
    done
    cat <<EOF > .envrc
export \$(nix run ''${pkgs[@]} -c "env" | grep ^PATH)
EOF

    direnv allow
  '';

  mkshell = pkgs.writeShellScriptBin "mkshell" ''
    set -ue
    set -o pipefail
    force=''${1:-""}
    [ $force == "-f" ] && rm -f shell.nix && shift 1
    [ -f shell.nix ] && echo "shell.nix already exists" && exit 1
    cat <<EOF > shell.nix
let
  pkgs = import <nixpkgs> {};
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      ''${@}
    ];
  }
EOF
    cat <<EOF > .envrc
use_nix
EOF

    direnv allow
  '';

  mkvenv = pkgs.writeShellScriptBin "mkvenv" ''
    set -ue
    set -o pipefail

    version=''${1:-3}

    cat <<EOF > shell.nix
let pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  buildInputs = with pkgs; [
    (python''${version}.withPackages (ps: [ ps.pip ps.setuptools ]))
  ];
  shellHook = '''
    alias pip="PIP_PREFIX='\$(pwd)/_build/pip_packages' pip"
    export PYTHONPATH="\$(pwd)/_build/pip_packages/\''${pkgs.python''${version}.sitePackages}:\$PYTHONPATH"
    unset SOURCE_DATE_EPOCH
  ''';
}
EOF
  '';

in {

  home.packages = with pkgs; [
    mkshell mkenv mkvenv
    zsh-completions
    gopass
  ];

  programs.git = {
    enable = true;
    aliases = {
      amd = "commit --amend -a --no-edit";
      st = "status";
    };
    userEmail = "eon@patapon.info";
    userName = "Jean-Philippe Braun";
    extraConfig = {};
  };

  programs.gpg.enable = true;

  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
    maxCacheTtl = 28800;
  };

  programs.password-store = {
    enable = true;
    package = pkgs.pass-wayland.withExtensions (ext: [ ext.pass-otp ]);
    settings.PASSWORD_STORE_DIR = "$HOME/vcs/password-store";
  };

  programs.pazi.enable = true;

  programs.fzf.enable = true;

  programs.direnv = {
    enable = true;
    stdlib = builtins.readFile ./conf/direnvrc;
  };

  programs.starship = {
    enable = true;
    settings = {
      scan_timeout = 10;
      format = ''$username$hostname$directory$git_branch$git_state$git_status$nix_shell$kubernetes$custom$line_break$jobs$battery$character'';
      character = {
        success_symbol = "[](bold green)";
        error_symbol = "[✗](bold red)";
        use_symbol_for_status = true;
      };
      directory = {
        truncation_length = 0;
        truncate_to_repo = false;
        style = "bold blue";
      };
      nix_shell.format = "in [❄️ nix-shell]($style) ";
      kubernetes = {
        disabled = false;
      };
      custom.kswitch = {
        command = "echo ";
        when = ''kswitch --json | jq -e -r '. | select(.tunnel.status == "up") | select(.context == .tunnel.zone) | .context' '';
        format = "[$output]($style)";
        style = "bold white";
      };
    };
  };

  programs.zsh = {
    enable = true;
    inherit sessionVariables shellAliases;
    enableCompletion = true;
    enableAutosuggestions = true;
    autocd = true;
    dotDir = ".config/zsh";
    initExtra = initExtraCommon + ''
      precmd_functions+=(set_win_title)
      autoload -Uz compinit && compinit
      autoload bashcompinit && bashcompinit
      zstyle ':completion:*' menu select

      # add /
      WORDCHARS=''${WORDCHARS/\/}

      bindkey -e

      # ctrl u
      bindkey \^U backward-kill-line
      # home
      bindkey "\e[H" beginning-of-line
      # end
      bindkey "\e[F" end-of-line
      # alt backspace
      bindkey "\^h" backward-kill-word
      # ctrl left/right
      bindkey $terminfo[kRIT5] forward-word
      bindkey $terminfo[kLFT5] backward-word

      source ${pkgs.zsh-syntax-highlighting}/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    '';
  };

  programs.bash = {
    enable = true;
    inherit sessionVariables shellAliases;
    historyControl = [ "erasedups" "ignorespace" ];
    initExtra = initExtraCommon + ''
      starship_precmd_user_func="set_win_title"
      PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
    '';
  };

  xdg.configFile."termite/config".source = ./conf/termite;
  xdg.configFile."termite/config-white".source = ./conf/termite-white;
  xdg.configFile."alacritty/alacritty.yml".source = ./conf/alacritty.yml;

  xdg.configFile."tig/config".text = ''
    color cursor white blue bold
  '';

}
