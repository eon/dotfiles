{ config, pkgs, ... }: {

  services.mpd = {
    enable = true;
    musicDirectory = "/media/music";
    extraConfig = ''
      audio_output {
        type        "pulse"
        name        "MPD"
      }
      max_output_buffer_size "16384"
    '';
  };

  xdg.configFile."ncmpcpp/config".text = ''
    media_library_primary_tag = album_artist
  '';

  services.mpdris2.enable = false;

}
