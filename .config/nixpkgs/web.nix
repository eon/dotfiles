{ config, pkgs, ... }:

{

  home.packages = with pkgs; [
    google-chrome
  ];

  programs.bash.sessionVariables = {
    MOZ_ENABLE_WAYLAND = 1;
  };

  programs.firefox = {
    enable = true;
    extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      https-everywhere
      privacy-badger
      browserpass
      close-other-windows
      octotree
      ublock-origin
      tab-center-redux
    ];
    profiles = {
      default = {
        id = 0;
        settings = {
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
          "widget.wayland-dmabuf-webgl.enabled" = true;
          "widget.wayland-dmabuf-vaapi.enabled" = true;
          "widget.wayland-dmabuf-textures.enabled" = true;
          "gfx.webrender.all" = true;
          # "layers.acceleration.force-enabled" = true;
        };
        userChrome = ''
          #TabsToolbar {
              display: none !important;
              visibility: hidden !important;
          }
        '';
      };
    };
  };

  programs.chromium = {
    enable = true;
    extensions = [
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock
    ];
  };

  programs.browserpass.enable = true;

}
